import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.lang.reflect.*;


class Singleton{
    public String str;

    private Singleton(){

    }

    //create an object of SingleObject
    private static Singleton instance;



    //Get the only object available
    public static Singleton getSingleInstance(){
        if (instance == null){
            instance = new Singleton();
        }
        return instance;
    }


}
